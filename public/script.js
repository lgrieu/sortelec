$( function() {
    $( ".sortable" ).sortable({
      placeholder: "ui-state-highlight"
    });
    $( ".sortable" ).disableSelection();
  } );
  
        
    function SavePage() {  
        html2canvas(document.getElementById('Page')).then(function(canvas) {
        document.body.appendChild(canvas);
        canvas.id = "PageCopy";
        canvas.style.display ="none";
        var link = document.createElement('a');
        var dataURL = canvas.toDataURL("image/png");
        link.download = 'image.png';
        link.href = dataURL; 
        document.body.appendChild(link);
        link.style.display ="none";  
        link.click();
        document.body.removeChild(link);
        document.body.removeChild(canvas);
        });
    }
